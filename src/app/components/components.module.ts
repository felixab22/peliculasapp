import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SlidesshowBackdropComponent } from './slidesshow-backdrop/slidesshow-backdrop.component';
import { IonicModule } from '@ionic/angular';
import { PipesModule } from '../pipes/pipes.module';
import { SlidesshowPosterComponent } from './slidesshow-poster/slidesshow-poster.component';



@NgModule({
  declarations: [
    SlidesshowBackdropComponent,
    SlidesshowPosterComponent,
  ],
  imports: [
    CommonModule,
    IonicModule.forRoot(), 
    PipesModule,
  ],
  exports:[
    SlidesshowBackdropComponent,
    SlidesshowPosterComponent,
  ]
})
export class ComponentsModule { }
