import { Component, OnInit, Input } from '@angular/core';
import { PeliculasModel } from 'src/app/model/movie.moel';

@Component({
  selector: 'app-slidesshow-backdrop',
  templateUrl: './slidesshow-backdrop.component.html',
  styleUrls: ['./slidesshow-backdrop.component.scss'],
})
export class SlidesshowBackdropComponent implements OnInit {
  @Input() peliculas:PeliculasModel[] = [];
  slideOpts = {
    slidesPerView: 1.3,
    freeMode: true
  };
  constructor() { }

  ngOnInit() {}
  
}
