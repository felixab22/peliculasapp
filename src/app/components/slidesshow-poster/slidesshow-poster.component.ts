import { Component, OnInit, Input } from '@angular/core';
import { PeliculasModel } from 'src/app/model/movie.moel';

@Component({
  selector: 'app-slidesshow-poster',
  templateUrl: './slidesshow-poster.component.html',
  styleUrls: ['./slidesshow-poster.component.scss'],
})
export class SlidesshowPosterComponent implements OnInit {
  @Input() posters:PeliculasModel[] = [];
  slideOpts = {
    slidesPerView: 3.2,
    freeMode: true
  };
  constructor() { }

  ngOnInit() {}

}
