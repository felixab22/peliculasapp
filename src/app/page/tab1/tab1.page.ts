import { Component, OnInit } from '@angular/core';
import { MoviesService } from '../../services/movies.service';
import { PeliculasModel } from '../../model/movie.moel';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit {
peliculasRec:PeliculasModel[] = [];

slideOpts = {
  slidesPerView: 1.3,
  freeMode: true
};

  constructor(
    private _MovieSrv: MoviesService
  ) {

  }

  ngOnInit(){
    this.listatMovie();
  }
  listatMovie(){
    this._MovieSrv.getFeature().subscribe(res =>{
      console.log(res);
      this.peliculasRec = res.results;
    });
  }
}
